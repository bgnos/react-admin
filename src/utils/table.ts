import moment from 'moment';

// 金额保留两位小数格式化
export const amountRender = (amount: string | number) =>
    isNaN(Number(amount)) ? amount : Number(amount).toFixed(2);

// 年-月-日
export const dateRender = (value: Date | string) => value && moment(value).format('YYYY-MM-DD');

// // 年-月-日 时：分：秒
export const dateTimeRender = (value: Date | string) =>
    value && moment(value).format('YYYY-MM-DD HH:mm:SS');
