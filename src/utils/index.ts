import { message } from 'antd';

export * from './table';

export const fetchData = async (
    api: (params?: object) => Promise<any>,
    params?: object,
    isLoading = true
) => {
    // if (isLoading) Loading.show();
    return api(params)
        .then((result) => {
            if (result.code == '0') {
                // Loading.close();
                return Promise.resolve(result?.data);
            } else {
                Message.error(result.message);
                return Promise.reject(result);
            }
        })
        .catch((err) => {
            // Loading.close();
            throw err;
        });
};

export const Message = {
    ...message,
    success: (msg: string) => {
        message.destroy();
        return message.success(msg);
    },
    warning: (msg: string) => {
        message.destroy();
        return message.warning(msg);
    },
    error: (msg: string) => {
        message.destroy();
        return message.error(msg);
    },
    info: (msg: string) => {
        message.destroy();
        return message.info(msg);
    },
};

export default { Message };
