import React from "react";
import { Layout, Menu, Dropdown } from "antd";
const { Header } = Layout;

import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

import UserDropdown from "./UserDropdown";
import style from "./layout.module.scss";

interface LayoutHeaderProps {
  collapsed: boolean;
  setCollapsed: (value: boolean) => any;
}

const origin = import.meta.env.VITE_OLD_CRM_HTTP;

let defaultTopMenuActive = "";
const TOP_MENUS = [
  {
    label: "销售部",
    key: origin + "Account/AccountSearch.asp",
  },
  {
    label: "培训部",
    key: origin + "crmplus/train/#/training",
  },
  {
    label: "测评部",
    key: origin + "CePing/AccountSearch.asp",
  },
  {
    label: "财务部",
    key: origin + "crmplus/finance/#/finance",
  },
  {
    label: "网站部",
    key: origin + "task/inetSearch.asp",
  },
  {
    label: "合同审查部",
    key: origin + "ManagerFun/CkIndex.asp",
  },
  {
    label: "快递",
    key: origin + "ExpressNew/NewCPExpressMission/index",
  },
  {
    label: "校园部",
    key: origin + "ncrm/#/Campus/Customer/CustomerSearch",
  },
  {
    label: "标准化项目订单",
    key: "1",
  },
  {
    label: "系统",
    key: origin + "Account/AccountSearch.asp",
  },
  {
    label: "帮助",
    key: origin + "help/PageAuthorityHelp.asp",
  },
].map(({ key, label }: { key: string; label: React.ReactNode }, index) => {
  if (label === "标准化项目订单") {
    defaultTopMenuActive = String(index);
  } else {
    label = <a href={key}>{label}</a>;
  }
  return { key: index, label };
});

function LayoutHeader(props: LayoutHeaderProps) {
  return (
    <Header className={style.header}>
      {React.createElement(
        props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
        {
          className: style["collapsed-icon"],
          onClick: () => props.setCollapsed(!props.collapsed),
        }
      )}
      <Menu
        className={style["header-menu"]}
        mode="horizontal"
        selectedKeys={[defaultTopMenuActive]}
        items={TOP_MENUS}
      />

      <UserDropdown />
    </Header>
  );
}

export default LayoutHeader;
