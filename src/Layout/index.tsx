import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';

import { Layout } from 'antd';
const { Content } = Layout;

import PrivateRoute from '@/routes/privateRoute';
import LayoutHeader from '@/Layout/LayoutHeader';
import LayoutSider from '@/Layout/LayoutSider';
import style from './layout.module.scss';

const App: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);

    return (
        <PrivateRoute>
            <Layout className={style.layout}>
                <LayoutSider collapsed={collapsed} />
                <Layout className={style['site-layout']}>
                    <LayoutHeader collapsed={collapsed} setCollapsed={setCollapsed} />
                    <Content className={style.main}>
                        <Outlet />
                    </Content>
                </Layout>
            </Layout>
        </PrivateRoute>
    );
};

export default App;
