import React from "react";
import { Link } from "react-router-dom";

import type { MenuProps } from "antd";
import { Layout, Menu } from "antd";
const { Sider } = Layout;

import { ProjectOutlined, ShopOutlined } from "@ant-design/icons";

import Logo from "@/assets/imgs/logo.png";
import LogoHand from "@/assets/imgs/logo_hand.png";
import style from "./layout.module.scss";
// import { ItemType } from "antd/es/menu/hooks/useItems";

interface LayoutSiderProps {
  collapsed: boolean;
}

interface MenuItem {
  label: string;
  path?: string;
  icon?: React.ReactNode;
  children?: MenuItem[];
}

const menus: MenuItem[] = [
  {
    label: "项目管理",
    icon: <ProjectOutlined />,
    children: [
      {
        label: "线索列表",
        path: "/clue/list",
      },
      {
        label: "借支列表",
        path: "/borrow/list",
      },
      {
        label: "还款列表",
        path: "/repay/list",
      },
      {
        label: "报销申请列表",
        path: "/settle/list",
      },
      {
        label: "报销支付列表",
        path: "/settlepay/list",
      },
      {
        label: "交接清单",
        path: "/delivery/list",
      },
    ],
  },
  {
    label: "供应商管理",
    icon: <ShopOutlined />,
    path: "/supplier/list",
  },
];

function getItems(config: MenuItem[]): MenuProps["items"] {
  return config.map((item) => {
    return {
      ...item,
      children: item.children ? getItems(item.children) : undefined,
      label: item.children ? (
        item.label
      ) : (
        <Link to={item.path as string}>{item.label}</Link>
      ),
      key: item.path || item.label,
      test: "sdfsdfsdf",
    };
  });
}

const items: MenuProps["items"] = getItems(menus);

function LayoutSider(props: LayoutSiderProps) {
  return (
    <Sider
      className={style.sider}
      collapsible={false}
      collapsed={props.collapsed}
    >
      <div className={style["logo-box"]}>
        <img
          className={props.collapsed ? style["logo-collapsed"] : style.logo}
          src={props.collapsed ? LogoHand : Logo}
        />
      </div>
      <Menu mode="inline" items={items} />
    </Sider>
  );
}

export default LayoutSider;
