import { Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { useAppSelector, useAppDispatch } from "@/store";
import { removeUserInfo } from "@/store/auth/authSlice";

import style from "./layout.module.scss";
import Avatar from "@/assets/imgs/avatar.png";

export default () => {
  const { cname } = useAppSelector((state) => state.auth.userInfo) || {};
  const dispatch = useAppDispatch();

  return (
    <Dropdown
      menu={{
        items: [
          {
            key: "1",
            label: (
              <Link to="/login" onClick={() => dispatch(removeUserInfo())}>
                退出登录
              </Link>
            ),
          },
        ],
      }}
    >
      <span className={style["user-dropdown"]}>
        <img className={style.avatar} src={Avatar} alt={cname} />
        <span className={style["user-name"]}>{cname}</span>
        <DownOutlined className={style.arrow} />
      </span>
    </Dropdown>
  );
};
