import ReactDOM from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/locale/zh_CN';

import Routes from '@/routes';

import store from './store';
import { Provider } from 'react-redux';

import 'reset-css';
import '@/style/index.scss';

const router = createBrowserRouter(Routes);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <ConfigProvider locale={zhCN}>
        <Provider store={store}>
            <RouterProvider router={router} />
        </Provider>
    </ConfigProvider>
);
