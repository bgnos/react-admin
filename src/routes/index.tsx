import { Navigate } from "react-router-dom";

import Layout from "@/Layout";
import Welcome from "@/views/Welcome";
import Login from "@/views/Login";

import SettleRoutes from "./settle";
import ClueRoutes from "./clue";

const routes = [
  {
    path: "/",
    element: <Navigate to="/welcome" />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/",
    element: <Layout />,
    errorElement: <div>Dang!</div>,
    children: [
      ...ClueRoutes,
      ...SettleRoutes,
      {
        path: "/welcome",
        element: <Welcome />,
      },
    ],
  },
];

export default routes;
