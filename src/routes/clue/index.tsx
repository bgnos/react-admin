import Outlet from "@/components/OutLet";
import SettleList from "@/views/Clue/List";

export default [
  {
    path: "/clue",
    element: <Outlet />,
    children: [
      {
        path: "list",
        element: <SettleList />,
      },
    ],
  },
];
