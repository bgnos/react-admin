import { Navigate } from "react-router-dom";
import { useAppSelector } from "@/store";

const PrivateRoute = (props: { children: React.ReactNode }): JSX.Element => {
  const userInfo = useAppSelector((state) => state.auth.userInfo);

  const { children } = props;
  const isLoggedIn: boolean = !!(userInfo && userInfo.authtoken);

  return isLoggedIn ? <>{children}</> : <Navigate replace={true} to="/login" />;
};

export default PrivateRoute;
