import Outlet from "@/components/OutLet";
import SettleList from "@/views/Settle/List";

export default [
  {
    path: "/settle",
    element: <Outlet />,
    children: [
      {
        path: "list",
        element: <SettleList />,
      },
    ],
  },
];
