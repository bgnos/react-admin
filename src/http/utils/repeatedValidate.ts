import axios,{AxiosRequestConfig} from "axios";
import QS from "qs";

// 当前请求队列存储容器
const pendingRequest = new Map();

// 用于根据当前请求的信息，生成请求 Key；
function generateReqKey(config:AxiosRequestConfig) {
  const { method, url, params, data } = config;
  return [method, url, QS.stringify(params), QS.stringify(data)].join("&");
}

// 用于把当前请求信息添加到pendingRequest对象中；
function addPendingRequest(config:AxiosRequestConfig) {
  const requestKey = generateReqKey(config);
  config.cancelToken =
    config.cancelToken ||
    new axios.CancelToken((cancel) => {
      if (!pendingRequest.has(requestKey)) {
        pendingRequest.set(requestKey, cancel);
      }
    });
}

// 检查是否存在重复请求，若存在则取消已发的请求。
function removePendingRequest(config:AxiosRequestConfig) {
  const requestKey = generateReqKey(config);
  if (pendingRequest.has(requestKey)) {
    const cancelToken = pendingRequest.get(requestKey);
    cancelToken(requestKey);
    pendingRequest.delete(requestKey);
  }
}

export { addPendingRequest, removePendingRequest };
