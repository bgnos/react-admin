import { AxiosInstance } from "axios";
import trim from "lodash/trim";

// 合并 URL 项
// 自动去除多余的 /
const combineUrl = (...urls: string[]) =>
  urls.map((url) => trim(url, "/")).join("/");

/**
 * serviceUrl 微服务URL前缀
 */
export default (service: AxiosInstance, serviceUrl: string) => ({
  /**
   * put方法，对应put请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  put: (url: string, params?: object) =>
    service.put(combineUrl(serviceUrl, url), params),
  /**
   * post方法，对应post请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  post: (url: string, params?: object) => {
    return service.post(combineUrl(serviceUrl, url), params);
  },

  /**
   * get方法，对应get请求
   * @param {String} url [请求的url地址]
   * @param {Object} params [请求时携带的参数]
   */
  get: (url: string, params?: object) => {
    return service.get(combineUrl(serviceUrl, url), { params });
  },

  /**
   * 上传文件方法
   * @param {String} url 请求的URL地址
   * @param {Form} data 请求的multipart/form-data
   * @returns 请求 Promise
   */
  upload: (url: string, data: object) =>
    service.request({
      params: { crm_service_http_type: "upload" },
      url: combineUrl(serviceUrl, url),
      data: data,
    }),

  blobPost: (url: string, params?: object) =>
    service.post(combineUrl(serviceUrl, url), params, { responseType: "blob" }),

  blobGet: (url: string, params?: object) =>
    service.get(combineUrl(serviceUrl, url), { params, responseType: "blob" }),
});
