import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { removeCookies } from '@/store/auth/authSlice';

import { Message } from '@/utils';
import { addPendingRequest, removePendingRequest } from './repeatedValidate';
import { userInfoKey, getCookie } from '@/store/auth/authSlice';

// axios 示例默认配置
const serviceDefaultConfig: AxiosRequestConfig = {
    baseURL: import.meta.env.VITE_BASE_URL, // url = base url + request url
    timeout: 60000, // request timeout
    withCredentials: true,
    headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        Pragma: 'no-cache',
    },
};

function serviceGenerator(serviceConfig: AxiosRequestConfig = serviceDefaultConfig): AxiosInstance {
    // 构造
    const service = axios.create(serviceConfig);

    // --------------------- request-interceptors ----------------------- //

    service.interceptors.request.use((config: AxiosRequestConfig) => {
        // 添加 auth token
        const userInfo = getCookie(userInfoKey);
        const token = userInfo?.authtoken;
        if (token) {
            if (config.headers) config.headers['authToken'] = token;
        }

        // 上传文件时更改 Content-Type
        if (config.params && config.params.crm_service_http_type == 'upload') {
            config.method = 'post';
            if (config.headers) config.headers['Content-Type'] = 'multipart/form-data';
        }
        removePendingRequest(config);
        addPendingRequest(config);
        return config;
    });

    // --------------------- request-response ----------------------- //

    service.interceptors.response.use(
        (response: AxiosResponse) => {
            removePendingRequest(response.config);
            // 检查 body code
            if (response.status !== 200) {
                return Promise.reject(new Error(response.data.message || 'Error'));
            }
            // 返回 body
            return response.data;
        },
        // 异常处理
        (error) => {
            Message.destroy();
            if (error.response) {
                const data = error.response;
                //尚未登录
                if ([480, 481, 482, 483, 484, 485].indexOf(data.status) > -1) {
                    Message.warning('登录信息不匹配，请重新登录！');
                    // store.commit("authStore/updateUserInfo", null); // FIXME 未清除
                    removeCookies();
                    location.href = '/login';
                } else if (data.status == 486) {
                    Message.warning('无权操作！');
                    location.href = '/403';
                } else {
                    Message.error('系统错误，请稍候再试！');
                }
            } else {
                if (axios.isCancel(error)) {
                    console.log('已取消的重复请求：' + error.message);
                } else {
                    Message.error('系统错误，请稍候再试！');
                }
            }
            removePendingRequest(error.config || {});
            return Promise.reject(error);
        }
    );

    return service;
}

export const service = serviceGenerator();
export default serviceGenerator;
