import { Modal, ModalProps } from 'antd';

interface PropsType extends ModalProps {
    setOpen: (open: boolean) => void;
}

export default (props: PropsType) => {
    const handleOk = () => props.setOpen(false);
    const handleCancel = () => props.setOpen(false);

    return <Modal title="选择项目" onCancel={handleCancel} {...props} onOk={handleOk}></Modal>;
};
