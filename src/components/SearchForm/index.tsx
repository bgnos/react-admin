import React, { useEffect } from 'react';

export default (props: { children: React.ReactNode; container: string; item: string }) => {
    const container = document.querySelector(props.container);
    const items = container && container.querySelectorAll(`${props.container}>${props.item}`);

    const eventHandler = () =>
        setMargin(container as HTMLElement, items as NodeListOf<HTMLElement>);

    useEffect(() => {
        if (!container || !items?.length) return;
        eventHandler();
        window.addEventListener('resize', eventHandler);
        return () => {
            window.removeEventListener('resize', eventHandler);
        };
    });

    return <>{props.children}</>;
};

function setMargin(container: HTMLElement, items: NodeListOf<HTMLElement>) {
    if (!container || !items.length) return;

    const item: HTMLElement = items[0];
    const containerWidth = container.offsetWidth - 20;
    let itemNum = Math.floor(containerWidth / item.offsetWidth);
    if (itemNum > items.length) {
        itemNum = items.length;
    }

    const leftWidth = containerWidth - itemNum * item.offsetWidth;
    const marginWidth = leftWidth / (itemNum + 1);
    Array.prototype.forEach.call(items, (item, index) => {
        if ((index + 1) % itemNum === 0) {
            item.style.marginLeft = marginWidth + 'px';
            item.style.marginRight = marginWidth + 'px';
        } else {
            item.style.marginLeft = marginWidth + 'px';
            item.style.marginRight = '0px';
        }
    });
}
