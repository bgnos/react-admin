import React from "react";
import { Collapse } from "antd";

const { Panel } = Collapse;

import "./index.module.scss";

export default (props: {
  title: React.ReactNode;
  children: React.ReactNode;
}) => {
  return (
    <Collapse
      className="crm-collapse"
      defaultActiveKey={["1"]}
      expandIconPosition="end"
    >
      <Panel header={props.title} key="1">
        {props.children}
      </Panel>
    </Collapse>
  );
};
