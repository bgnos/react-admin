import React from "react";
import { useNavigate } from "react-router-dom";

import "./index.module.scss";

export const backBtn = () => {
  const navigate = useNavigate();
  return <a onClick={() => navigate(-1)}>返回</a>;
};

export default (props: {
  title?: React.ReactNode;
  children?: React.ReactNode;
  topRightBtn?: React.ReactNode;
}) => {
  return (
    <div className="panel-container">
      <div className="panel-title-container">
        {typeof props.title === "string" ? (
          <div className={`"panel-title" ${props.topRightBtn ? "flex" : ""}`}>
            <span>{props.title}</span>
            {props.topRightBtn}
          </div>
        ) : (
          props.title
        )}
      </div>
      <div className={`content ${props.title ? "content-border" : ""}`}>
        {props.children}
      </div>
    </div>
  );
};
