import { Button, Form, Input } from "antd";
import { useNavigate } from "react-router-dom";

import { useAppDispatch } from "@/store";
import { setUserInfo } from "@/store/auth/authSlice";
import { login } from "@/api/auth";
import { fetchData } from "@/utils";

import Logo from "@/assets/imgs/logo.png";
import Login from "@/assets/imgs/Login.png";

import style from "./index.module.scss";

export default function () {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const onFinish = (values: any) => {
    fetchData(login, values).then((data) => {
      dispatch(setUserInfo(data));
      navigate("/");
    });
  };

  return (
    <div className={style["login-container"]}>
      <div className={style.form}>
        <img className={style.logo} src={Logo} alt="logoCrm" />
        <div className={style.welcome}>欢迎登录</div>

        <Form
          name="login"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <Button
              className={style["login-btn"]}
              type="primary"
              htmlType="submit"
            >
              登录
            </Button>
          </Form.Item>
        </Form>
      </div>
      <img className={style["login-bg"]} src={Login} alt="login" />
    </div>
  );
}
