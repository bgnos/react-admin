import Welcome from '@/assets/imgs/welcome.png';
import style from './index.module.scss';

// import { pick } from '@51job/jbs-utils';

// const obj = {
//     name: 'andy',
//     age: 18,
//     car: 'audi',
// };

// const res = pick(obj, ['name']);

export default function () {
    return (
        <>
            <div className={style['welcome-container']}>
                <div className={style.left}>欢迎使用CRM系统-标准项目订单</div>
                <img className={style.right} src={Welcome} alt="" />
            </div>
        </>
    );
}
