import { Link } from 'react-router-dom';
import type { ColumnsType } from 'antd/es/table';
import { DataType } from './type';
import { dateRender, amountRender } from '@/utils';

export const defaultForm = {
    proType: null, // 项目类型
    proNumber: '', // 项目编号
    proName: '', // 项目名称
    branch: null, // 城市
    applyStatus: null, // 结算状态
    applyer: '', // 报销申请人姓名

    applyTimeStart: '', // 报销申请开始时间
    applyTimeEnd: '', // 报销申请结束时间时间
    totalAmountStart: undefined, // 查询总报销金额起始值
    totalAmountEnd: undefined, // 查询总报销金额结束值
};

export const defaultOptions = {
    applyStatusCondition: {
        clearable: false,
        options: [],
    },
    branchCondition: {
        clearable: false,
        options: [],
    },
    proTypeCondition: {
        clearable: false,
        options: [],
    },
};

export const columns: ColumnsType<DataType> = [
    {
        dataIndex: 'settleApplyId',
        title: '报销编号',
        render: (text) => <Link to="/settle/detail">{text}</Link>,
    },
    {
        dataIndex: 'proNumber',
        title: '项目编号',
        // render: (text) => <a>{text}</a>,
    },
    {
        dataIndex: 'proName',
        title: '项目名称',
    },
    {
        dataIndex: 'applyStatus_label',
        title: '申请状态',
    },
    {
        dataIndex: 'branch_label',
        title: '城市',
    },
    {
        dataIndex: 'applyerCname',
        title: '申请人',
    },
    {
        dataIndex: 'applyTime',
        title: '申请时间',
        render: dateRender,
    },
    {
        dataIndex: 'totalAmount',
        title: '报销总额',
        render: amountRender,
    },
];
