export interface Option {
    code: string;
    value: string;
}

export interface OptionsMapItem {
    clearable: boolean;
    options: Option[];
}

export interface OptionsMap {
    applyStatusCondition: OptionsMapItem;
    branchCondition: OptionsMapItem;
    proTypeCondition: OptionsMapItem;
}

export interface DataType {
    settleApplyId: string;
    proNumber: string;
    proName: string;
    applyStatus_label: string;
    branch_label: string;
    applyerCname: string;
    applyTime: string;
    totalAmount: string | number;
}
