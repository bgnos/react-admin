import { useState } from 'react';
import { Button, Table } from 'antd';

import QueryForm from './QueryForm';
import CrmPanel from '@/components/CrmPanel';
import SelectProjectModal from '@/components/SelectProjectModal';

import { fetchData } from '@/utils';
import { getSettleApplyBatchList } from '@/api/projectManager';

import { columns } from './config';

const pagination = {
    showTotal: (total: number | string) => `共 ${total} 条`,
    showQuickJumper: true,
    pageSizeOptions: [10, 20, 50],
};

function SettleList() {
    const [list, setList] = useState([]);
    const [visible, setVisible] = useState(false);

    const getSettleList = (form: object) => {
        fetchData(getSettleApplyBatchList, {
            ...form,
            pageNum: 1,
            pageSize: 10,
        }).then((data) => {
            setList(data.list);
        });
    };

    return (
        <>
            <QueryForm getSettleList={getSettleList}></QueryForm>
            <CrmPanel
                title="报销申请列表"
                topRightBtn={<Button onClick={() => setVisible(true)}>新增</Button>}
            >
                <Table
                    rowKey="settleApplyId"
                    columns={columns}
                    dataSource={list}
                    pagination={pagination}
                />
            </CrmPanel>
            <SelectProjectModal open={visible} setOpen={setVisible} />
        </>
    );
}

export default SettleList;
