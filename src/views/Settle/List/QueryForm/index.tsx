import { useEffect, useState } from 'react';
import { Button, Form, Input, Select } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import { cloneDeep } from 'lodash';

import { fetchData } from '@/utils';
import { getSettleBatchApplyListCondition } from '@/api/common';

import CrmCollapse from '@/components/CrmCollapse';
import SearchForm from '@/components/SearchForm';

import { Option, OptionsMap } from '../type';
import { defaultForm, defaultOptions } from '../config';

function formatOption({ code, value }: Option) {
    return {
        value: code,
        label: value,
    };
}

export default function (props: { getSettleList: (form: object) => void }) {
    const [options, setOptions] = useState<OptionsMap>(defaultOptions);
    const [initialValues, setInitialValues] = useState(defaultForm);

    const [form] = Form.useForm();

    const getOption = () => {
        fetchData(getSettleBatchApplyListCondition).then((data: OptionsMap) => {
            setOptions(data);

            const initialValues = cloneDeep(defaultForm);
            Object.keys(data).forEach((key) => {
                const field = key.replace('Condition', '');
                if (data)
                    if (data[key].clearable) {
                        initialValues[field] = null;
                    } else {
                        initialValues[field] = data[key].options[0]?.code;
                    }
            });
            form.setFieldsValue(initialValues);
        });
    };

    useEffect(() => {
        getOption();
    }, []);

    return (
        <SearchForm container="#settle-search-form" item=".ant-form-item">
            <CrmCollapse title="查询条件">
                <Form
                    form={form}
                    id="settle-search-form"
                    className="search-form"
                    onFinish={props.getSettleList}
                    autoComplete="off"
                    initialValues={initialValues}
                >
                    <Form.Item label="项目类型" name="proType">
                        <Select
                            allowClear={options.proTypeCondition.clearable}
                            options={options.proTypeCondition.options.map(formatOption)}
                        />
                    </Form.Item>

                    <Form.Item label="项目编号" name="proNumber">
                        <Input />
                    </Form.Item>

                    <Form.Item label="项目名称" name="proName">
                        <Input />
                    </Form.Item>

                    <Form.Item label="申请人" name="applyer">
                        <Input />
                    </Form.Item>

                    <Form.Item label="城市" name="branch">
                        <Select
                            allowClear={options.branchCondition.clearable}
                            options={options.branchCondition.options.map(formatOption)}
                        />
                    </Form.Item>

                    <Form.Item label="结算状态" name="applyStatus">
                        <Select
                            allowClear={options.applyStatusCondition.clearable}
                            options={options.applyStatusCondition.options.map(formatOption)}
                        />
                    </Form.Item>

                    <Form.Item label="申请日期" name="1">
                        <Input />
                    </Form.Item>

                    <Form.Item label="报销金额" name="2">
                        <Input />
                    </Form.Item>

                    <Form.Item className="operation">
                        <Button>重置</Button>
                        <Button type="primary" htmlType="submit" icon={<SearchOutlined />}>
                            搜索
                        </Button>
                    </Form.Item>
                </Form>
            </CrmCollapse>
        </SearchForm>
    );
}
