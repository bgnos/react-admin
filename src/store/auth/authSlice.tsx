import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import Cookie from 'js-cookie';

const defaultUserInfo = {
    authtoken: '',
    branch: '',
    cname: '',
    directTargetCode: '',
    directTargetNum: '',
    ismanager: '',
    oprname: '',
    realoprbranch: '',
    role: '',
    serverbranch: '',
    sessionid: '',
};

interface UserInfo {
    authtoken: string;
    branch: string;
    cname: string;
    directTargetCode: string;
    directTargetNum: string;
    ismanager: string;
    oprname: string;
    realoprbranch: string;
    role: string;
    serverbranch: string;
    sessionid: string;
}

// 为 slice state 定义一个类型
interface AuthState {
    userInfo: UserInfo | null;
}

export const crmSysKey = 'CRM';
export const nCrmSysKey = 'NCRM-SERVICE';
export const userInfoKey = 'CRMPLUS';
export const JSESSIONIDKey = 'JSESSIONID';

// 使用该类型定义初始 state
const initialState: AuthState = {
    userInfo: getCookie(userInfoKey) || null,
};

//解析cookie
export function decodeCookie(value: string) {
    let userInfo: any = {};
    const decodeValueArr = decodeURIComponent(value).split('&');
    decodeValueArr.forEach((item) => {
        const splitItem = item.split('=');
        if (splitItem[0] == 'cname') {
            splitItem[1] = decodeURI(splitItem[1]);
        }
        userInfo[splitItem[0]] = splitItem[1];
    });
    return userInfo;
}
export function getCookie(name: string) {
    let value: string = Cookie.get(name) || '';
    let userInfo: UserInfo = { ...defaultUserInfo };
    if (value && value != '') {
        userInfo = decodeCookie(value);
        Cookie.set(JSESSIONIDKey, userInfo.sessionid);
    }
    return userInfo || {};
}

export function removeCookies() {
    Cookie.remove(userInfoKey);
    Cookie.remove(JSESSIONIDKey);
    Cookie.remove(crmSysKey);
    Cookie.remove(nCrmSysKey);
}

export const counterSlice = createSlice({
    name: 'Auth',
    initialState,
    reducers: {
        setUserInfo: (state, action: PayloadAction<string>) => {
            const userInfo: UserInfo = decodeCookie(action.payload);
            state.userInfo = userInfo;
            Cookie.set(userInfoKey, action.payload);
            Cookie.set(JSESSIONIDKey, state.userInfo.sessionid);
        },
        removeUserInfo: (state) => {
            state.userInfo = null;
            removeCookies();
        },
    },
});
// 每个 case reducer 函数会生成对应的 Action creators
export const { setUserInfo, removeUserInfo } = counterSlice.actions;

export default counterSlice.reducer;
