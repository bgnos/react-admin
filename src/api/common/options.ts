import { get } from '@/http';

//  报销批次列表查询条件选项
export const getSettleBatchApplyListCondition = () =>
    get('/search/getSettleBatchApplyListCondition');

// 费用类别选项
export const getFeeAmountTypeList = (params?: object) =>
    get('/search/getFeeAmountTypeList', params);

// 查询内部员工信息
export const getEhrInfo = (params?: object) => get('/search/getEhrInfo', params);

// 获取当前项目类型下的所有供应商信息，账户信息
export const getSupplier = (params?: object) => get('/search/selectSupplier', params);
