import { post } from '@/http';

// 根据关键字查询项目简单信息列表
export const selectProjectInfo = (params?: object) => post('/project/selectProjectInfo', params);

// 报销、借支申请等操作，查询项目信息及相关费用信息
export const getProjectWithFeeInfo = (params?: object) =>
    post('/project/getProjectWithFeeInfo', params);

// 更新项目实际线下收入
export const editFairAmount = (params?: object) => post('/project/editFairAmount', params);
