import { post } from '@/http/supplier';
import qs from 'qs';

// 供应商列表
export const getList = (params?: object) => post('list', params);

// 新增供应商
export const addSupplier = (params?: object) => post('add', params);
// 启用供应商
export const enable = (params?: object) => post('enable', params);
// 禁用供应商
export const disable = (params?: object) => post('disable', params);

// 查看附件列表

export const lookFile = (params?: object) => post('attachment/list', params);

// 供应商附件上传

export const uploadFile = (query: object, params: object) => {
    return post(`attachment/upload?${qs.stringify(query)}`, params);
};

// 供应商账号列表
export const getAccountList = (params?: object) => post('account/list', params);

// 启用供应商账号
export const enableAccount = (params?: object) => post('account/enable', params);

// 禁用供应商账号
export const disableAccount = (params?: object) => post('account/disable', params);

// 新增供应商账号
export const addSupplierAccount = (params?: object) => post('account/add', params);
