import { blobPost } from '@/http';

// 下载文件
export const downloadFile = (params?: object) => blobPost('pubFile/downloadFile', params);
