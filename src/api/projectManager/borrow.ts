import { post } from '@/http';

// 申请中的借支列表
export const getBorrowExpectList = (params?: object) => post('/borrow/borrowExpectList', params);

// 已申请借支列表
export const getBorrowActualList = (params?: object) => post('/borrow/borrowActualList', params);
