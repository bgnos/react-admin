import { get, post } from '@/http';

// 报销批次列表
export const getSettleApplyBatchList = (params?: object) =>
    post('/settle/getSettleApplyBatchList', params);

// 报销申请
export const settleApply = (params?: object) => post('/settle/settleApply', params);

// 报销详情
export const gettSettleApplyBatchDetail = (params?: object) =>
    get('/settle/selectSettleApplyBatchDetail', params);

// 报销邮件预览
export const settleApplyEmailPreview = (params?: object) =>
    post('/settle/settleApplyEmailPreview', params);
